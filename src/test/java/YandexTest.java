import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import business_objects.Email;
import business_objects.User;
import pages.*;


public class YandexTest {
    private static final String START_URL = "https://mail.yandex.by";

    private WebDriver driver;
    private User user = new User();
    private Email email = new Email();

    @BeforeClass(description = "Open YANDEX home page")
    public void openYandex() throws Exception {
        driver = drivers.Driver.getWebDriverInstance();
        driver.get(START_URL);

    }

    @Test(description = "Login to YANDEX")
    public void loginToYandex() {
        boolean loginIsComplete = new YandexSignInPage(driver)
                .loginToYandex(user.getUserName(), user.getPassword())
                .isDisplayed(user.getFullUserName());
        Assert.assertTrue(loginIsComplete, "Looks you are NOT logged in correctly");
    }

    @Test(dependsOnMethods = "loginToYandex", description = "Create draft email")
    public void createDraftMail() {
        boolean draftIsCreated = new YandexInboxPage(driver).navigateToDraftEmailPage().
                createNewMail(email.getRecipient(), email.getSubject(), email.getBody()).isEmailDisplayed(email.getSubject());
        Assert.assertTrue(draftIsCreated, "Email is NOT created");
    }

    @Test(dependsOnMethods = "createDraftMail", description = "Verify data in draft email")
    public void verifyDraftEmailData() {
        boolean recepientIsCorrect = new YandexEmailListPage(driver).navigateToDraftEmail(email.getSubjectLocator()).
                draftMailRecipientVerification(user.getRecipientDraftMail());
        Assert.assertTrue(recepientIsCorrect, "Recepient is NOT valid");

        boolean subjectIsCorrect = new YandexDraftEmailPage(driver).draftMailSubjectVerification(email.getSubject());
        Assert.assertTrue(subjectIsCorrect, "Subject is NOT valid");

        boolean bodyIsCorrect = new YandexDraftEmailPage(driver).draftMailBodyVerification(email.getBody());
        Assert.assertTrue(bodyIsCorrect, "Body is NOT valid");
    }

    @Test(dependsOnMethods = "verifyDraftEmailData", description = "Sent email")
    public void sentEmail() {
        boolean isEmailSent = new YandexDraftEmailPage(driver).sentEmail().isEmailSent();
        Assert.assertTrue(isEmailSent, "Email is NOT sent");
    }

    @Test(dependsOnMethods = "sentEmail", description = "Verify email is displayed in Sent email folder")
    public void verifySentFolder() {
        ;
        boolean sentEmailExist = new YandexInboxPage(driver).navigateToSentFolder().isEmailDisplayed(email.getSubject());
        Assert.assertTrue(sentEmailExist, "Email with the subject doesn't exist in the folder");
    }

    @Test(dependsOnMethods = "verifySentFolder", description = "Verify that email is deleted from sent folder")
    public void deleteSentEmailviaContextMenu() {
        if (driver.getClass().toString().contains("Chrome")) {
            new YandexEmailListPage(driver).deleteEmailViaContextMenu(email.getSubjectLocator());
            boolean sentEmailExist = new YandexInboxPage(driver).navigateToSentFolder().isEmailDisplayed(email.getSubject());
            Assert.assertTrue(!sentEmailExist, "Email with the subject exists in the folder");
        } else if (driver.getClass().getName().contains("firefox")) {
            System.out.println("deleteSentEmailviaContextMenu test is skipped for FireFox");
        }
    }

    @Test(dependsOnMethods = "deleteSentEmailviaContextMenu", description = "Verify email is NOT displayed in DRAFT folder")
    public void verifyDraftFolder() {
        boolean draftEmailExist = new YandexEmailListPage(driver).navigateToDraftFolder().
                isEmailDisplayed(email.getSubject());
        Assert.assertTrue(!draftEmailExist, "Email is displayed");
    }

    @Test(dependsOnMethods = "verifyDraftFolder", description = "Logout from YANDEX")
    public void logOff() {
        boolean isLoggOff = new YandexEmailListPage(driver).clickOnLogOff().isLoggOff();
        Assert.assertTrue(isLoggOff, "Looks you are NOT logged off correctly");
    }

    @AfterClass(description = "Stop Browser")
    public void stopBrowser() {
        driver.close();
        driver = null;
    }
}