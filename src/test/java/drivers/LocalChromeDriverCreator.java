package drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class LocalChromeDriverCreator implements DriverCreator{

    private final String filePath;

    public LocalChromeDriverCreator(String path) {
        filePath = path;
    }


    @Override
    public WebDriver createDriver() {
        System.out.println("Chrome driver");
        System.setProperty("webdriver.chrome.driver", filePath);
        return new ChromeDriver();
    }
}
