package util;


import org.apache.commons.lang3.RandomStringUtils;

public class StringGeneratorUtils {
    public static final int DEFAULT_INTERVAL_STRING = 90;

    public static String getRandomString(int maxValue) {
        return RandomStringUtils.randomAlphabetic(maxValue);
    }

    public static String getRandomString() {
        return getRandomString(DEFAULT_INTERVAL_STRING);
    }
}
